extern crate nom;
extern crate cse262_project;

use cse262_project::{program, run};

fn main() {
 let (unparsed, result) = program(r#"fn main() {
  return foo();
}
fn foo(){
  let x = 5;
  return x;
}"#).unwrap();



//let (unparsed, result) = program(r#"fn main(){return foo(1,2,3);} fn foo(a,b,c){return a+b+c;}"#).unwrap();
  //let (unparsed, result) = program(r#""hello world""#).unwrap();
  println!("{:?}", result);

  let output = run(&result);
  println!("{:?}", output);
}
